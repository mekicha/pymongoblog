### About Project

This is the final project for the course **MONGODB 101 for Python Developers** from the folks at
[MongoDB University](https://university.mongodb.com/)

It involves building a blog using the `bottle`  web framework and `pymongo` (MongoDB driver for Python)
Project files were provided by the course instructors.

### How to run

1. Run `python blog.py`. I did the tasks in using Python 2.7
2. Point your browser to `localhost:8082` to see the blog in all its glory :)
